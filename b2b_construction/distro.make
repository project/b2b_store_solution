; $ drush make sfdev_shop.make [directory]

api = 2
core = 7.x

projects[drupal][type] = core

;Profile
projects[b2b_construction][type] = profile
projects[b2b_construction][download][type] = git
projects[b2b_construction][download][url] = nortmas@git.drupal.org:sandbox/nortmas/1180258.git
projects[b2b_construction][directory_name] = "b2b_construction"