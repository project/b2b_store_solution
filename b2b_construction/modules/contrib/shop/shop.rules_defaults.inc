<?php
/**
 * @file
 * shop.rules_defaults.inc
 */

/**
 * Implementation of hook_default_rules_configuration().
 */
function shop_default_rules_configuration() {
  $items = array();
  $items['rules_discount'] = entity_import('rules_config', '{ "rules_discount" : {
      "LABEL" : "discount",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "commerce_line_item", "commerce_product_reference" ],
      "ON" : [ "commerce_product_calculate_sell_price" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "4" : "4" } }
          }
        }
      ],
      "DO" : [
        { "commerce_line_item_unit_price_subtract" : { "line_item" : [ "line_item" ], "amount" : "7" } }
      ]
    }
  }');
  return $items;
}
