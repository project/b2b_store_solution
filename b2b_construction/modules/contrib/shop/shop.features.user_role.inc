<?php
/**
 * @file
 * shop.features.user_role.inc
 */

/**
 * Implementation of hook_user_default_roles().
 */
function shop_user_default_roles() {
  $roles = array();

  // Exported role: Diller
  $roles['Diller'] = array(
    'name' => 'Diller',
    'weight' => '3',
  );

  return $roles;
}
