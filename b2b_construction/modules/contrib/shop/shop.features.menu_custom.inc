<?php
/**
 * @file
 * shop.features.menu_custom.inc
 */

/**
 * Implementation of hook_menu_default_menu_custom().
 */
function shop_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Основные ссылки',
    'description' => 'Меню основных ссылок показывает главные разделы сайта. Обычно выводятся в виде горизонтальной полосы вверху страницы.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Меню основных ссылок показывает главные разделы сайта. Обычно выводятся в виде горизонтальной полосы вверху страницы.');
  t('Основные ссылки');


  return $menus;
}
