<?php
/**
 * @file
 * shop.features.user_permission.inc
 */

/**
 * Implementation of hook_user_default_permissions().
 */
function shop_user_default_permissions() {
  $permissions = array();

  // Exported permission: access checkout
  $permissions['access checkout'] = array(
    'name' => 'access checkout',
    'roles' => array(
      0 => 'Diller',
      1 => 'administrator',
      2 => 'anonymous user',
      3 => 'authenticated user',
    ),
    'module' => 'commerce_checkout',
  );

  return $permissions;
}
